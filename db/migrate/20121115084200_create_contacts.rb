class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :id
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :state
      t.string :country
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
